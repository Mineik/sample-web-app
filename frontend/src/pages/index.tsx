import React from "react"
import Head  from "next/head"
import {ApolloProvider} from "@apollo/react-hooks";
import {MyClient} from "../apollo/apolloConf";
import TestComponent from "../components/TestComponent";


function Index(){
	return(
		<>
			<Head>
				<title>Testovací Stránka</title>
			</Head>
			<ApolloProvider client={MyClient}>
				<TestComponent />
			</ApolloProvider>
		</>
	)
}

export {Index as default}