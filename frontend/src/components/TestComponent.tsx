import React from "react";
import "../less/test.less"
import {useQuery} from "@apollo/react-hooks"
import {gql} from "apollo-boost"

export default function TestComponent(){
	const {loading, error, data} = useQuery(gql`
		{
			status{
				message
				code
			}
			database{
				message
			}
		}
	`)
	return(
		<div className={"statusBox"}>
			Načítání komponent: <span className={"works"}>Funguje</span> <br />
			Načítání Stylů: <span id={"technologyState"}>funguje</span> <br/>
			Připojení na backend: {	loading? "Načítám...":
									error? <span className={"fails"}>Nefunguje</span>:
									data?.status?.message? <span className={"works"}>{data.status.message}</span>:
									<span className={"fails"}>Nefunguje</span>}
			{data?.status?.code == 200 ? <><br/>Databáze: {
				data?.database?.message? <span className={"works"}>{data.database.message}</span> : <span className={"fails"}>Nefunguje</span>
			}</>: "" }
		</div>
	)
}