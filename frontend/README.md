# Sample frontend
2 easy steps how to run
1) `npm i`
1) `npm test`

To be sure everything works correctly, try setting up [Sample Backend](../backend/README.md) <br/>
For full project setup you can use Docker, described in [Project Readme](../README.md)

## Technology stack
This app runs Next.js with react.
It's built for styling using less and fetching data from GraphQL APIs using Apollo
