# Differences against previous versions
### v2.0
* started making changelog
* now the whole app can simply be run on graphQL
* obligatory updates to packages, highlighting
    * update to next.js 9.4, meaning new hot reload and ``pages`` inside `src` directory
* fix typo in ``next.config.js`` filename, I'm sorry
* Now Docker is imported into both projects by default (*functionality has to be tested yet*)


