# Sample Web project
You can build apps and shit on this

## Setup
### Frontend
* Run `npm i`
* now you can `npm test` or `npx next`

### Backend
* `npm i`
* `node .`

### Docker
Docker is now fully setup in project for two step process of app start
1) run `npm i` locally, so you don't run into any issues
2) run `docker-compose up` and wait for images to build and you should be up and running <br/>
(*tested under windows 10 Pro 1909 with Docker for desktop v2.3.0.3, Linux containers, HyperV backend*)

## IDEs
### Frontend
Fully set up for WebStorm and VSCode (workspace)

### Backend
Set up for VSCode (workspace)