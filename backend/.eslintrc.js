module.exports = {
	root: true,
	parser: "@typescript-eslint/parser", // Specifies the ESLint parser
	plugins: ["prettier", "sort-imports-es6-autofix"],
	extends: [
		"plugin:@typescript-eslint/recommended", // Uses the recommended rules from @typescript-eslint/eslint-plugin
		"prettier/@typescript-eslint", // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
		"plugin:prettier/recommended",
	],
	parserOptions: {
		ecmaVersion: 2019, // Allows for the parsing of modern ECMAScript features
		sourceType: "module", // Allows for the use of imports
	},
	rules: {
		"sort-imports-es6-autofix/sort-imports-es6": ["error"],
		"prettier/prettier": ["error"],
		"@typescript-eslint/explicit-function-return-type": ["off"],
	},
	overrides: [
		{
			files: ["**/*.tsx"],
			rules: {
				"react/prop-types": "off",
			},
		},
	],
}
