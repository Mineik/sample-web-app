import { ApolloServer, gql } from "apollo-server"
import { connVer } from "./databaseModule/bookshelf"
import { prepareTables } from "./databaseModule/installDB"

prepareTables()
let s
connVer
	.verifyIfWorks()
	.then(res => {
		s = res
		if (s !== "Funguje" && s !== "funguje") throw "e"
		else console.log("Data už v databázi!")
	})
	.catch(e => {
		console.log(e.message)
		console.log("Weird shit happenin")
		installBasicData()
	})

function installBasicData() {
	console.log("trying to post basic data to database")
	try {
		connVer.installData().catch(f => {
			throw f
		})
		console.log("Created basic data in database")
	} catch (g) {
		console.log("Error writing test data to database")
	}
}

const typeDefs = gql`
	# z nějakýho důvodu se to rozbije když se tenhle variable jmenuje jinak XD
	type Status {
		code: Int
		message: String
	}

	type DBConn {
		message: String
	}

	type Query {
		status: Status
		database: DBConn
	}
`

const stav = {
	code: 200,
	message: "🦄 Online",
}

const resolvers = {
	Query: {
		status: () => stav,
		database: async () => {
			console.log("Fetching Data")
			let gotMessage
			try {
				gotMessage = await connVer.verifyIfWorks()
				console.log(gotMessage)
			} catch (e) {
				console.log("Error Fetching data")
				console.log(e)
				gotMessage = "Nefunguje"
			}
			return {
				message: gotMessage,
			}
		},
	},
}

const server = new ApolloServer({ typeDefs, resolvers })
server.listen({ port: 3030 }).then(({ url }) => {
	console.log(`🚀 server ready at ${url}`)
})
