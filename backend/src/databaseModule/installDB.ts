import { knexConf } from "./conf"

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export async function prepareTables() {
	console.log("Creating Tables")
	const isTheTableHere = await knexConf.schema
		.hasTable("statusTestTable")
		.catch(e => {
			console.log(e)
			return true
		})

	console.log(isTheTableHere)
	if (!isTheTableHere) {
		console.log("Status table doesn't exist, generating...")
		knexConf.schema.createTable("statusTestTable", table => {
			table.increments("id").primary()
			table.integer("code")
			table.string("status")
		})
		console.log("Done")
	}
}
