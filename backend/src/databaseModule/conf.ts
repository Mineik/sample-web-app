import knex = require("knex")

export const knexConf = knex({
	client: "mysql",
	connection: {
		host: "database",
		user: "WABackend",
		password: "helloDocker",
		database: "WADb",
	},
})
// Client does not support authentication protocol requested by server; consider upgrading MySQL client
