import { knexConf } from "./conf"
import Bookshelf = require("bookshelf")

export const bookshelf = Bookshelf(knexConf)

export const connVer = bookshelf.model(
	"DBInfo",
	{
		tableName: "statusTestTable",
	},
	{
		verifyIfWorks: async function() {
			const idk = await this.forge({ code: 200 })
				.fetch()
				.catch(e => {
					console.log(e.message)
					console.log("Retrieving data from db Error")
					return "ssss"
				})
			return await idk.get("status")
		},

		installData: async function() {
			return await this.forge({
				code: 200,
				status: "Funguje",
			})
				.save()
				.catch(e => {
					console.log("Error")
				})
		},
	},
)
